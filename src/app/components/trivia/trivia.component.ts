import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trivia',
  templateUrl: './trivia.component.html',
  styleUrls: ['./trivia.component.css']
})
export class TriviaComponent implements OnInit {
  questions:Array<Object> = [
    {
      id: 1,
      question: "the first question",
      answers: [
        {
          id: 1,
          answer: "the first answer",
          istrue: true
        },
        {
          id: 2,
          answer: "the second answer",
          istrue: false
        },
        {
          id: 3,
          answer: "the third answer",
          istrue: false
        }
      ]
    },
    {
      id: 2,
      question: "the second question",
      answers: [
        {
          id: 1,
          answer: "the first answer",
          istrue: false
        },
        {
          id: 2,
          answer: "the second answer",
          istrue: true
        },
        {
          id: 3,
          answer: "the third answer",
          istrue: false
        }
      ]
    },
    {
      id: 3,
      question: "the third question",
      answers: [
        {
          id: 1,
          answer: "the first answer",
          istrue: false
        },
        {
          id: 2,
          answer: "the second answer",
          istrue: false
        },
        {
          id: 3,
          answer: "the third answer",
          istrue: true
        }
      ]
    }
  ]
  answers:Array<Number> = [];
  isCompleted:boolean = false;
  questionIndex:number = 0;
  constructor() { }

  ngOnInit() {
  }

  isChecked(id){
    return this.answers[this.questionIndex] === id;
  }

  checkAnswer(id) {
    this.answers[this.questionIndex.toString()]  = id;
    console.log(this.answers)
  }

  nextQuestion() {
    if(this.answers.length != this.questions.length) {
      this.questionIndex += 1;
    } else {
      this.isCompleted = true;
    }
  }

  previousQuestion() {
    if(this.questionIndex > 0) {
      this.questionIndex -= 1;
    }
  }

}
